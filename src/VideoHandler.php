<?php

namespace Drupal\video_toolbox;

use Drupal\Core\Database\Database;
use Drupal\Component\Utility\Random;
use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * The video Handler.
 */
class VideoHandler implements VideoHandlerInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The storage handler class for files.
   *
   * @var \Drupal\file\FileStorage
   */
  protected $fileStorage;

  /**
   * The Messenger Serivce.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritDoc}
   */
  public function __construct(
    Connection $connection,
    EntityTypeManagerInterface $entity_type_manager,
    MessengerInterface $messenger
  ) {
    $this->connection = $connection;
    $this->fileStorage = $entity_type_manager->getStorage('file');
    $this->messenger = $messenger;
  }

  /**
   * {@inheritDoc}
   */
  public function saveVideoInfo($description, $video, $user_id, $fid, $status, $time = NULL) {
    $database = $this->connection->insert('video_store');
    $database->fields(
      [
        'description' => $description,
        'video_key' => $video,
        'uid' => $user_id,
        'fid' => $fid,
        'status' => $status,
        'upload_date' => $time ?? time(),
      ]
    )->execute();
  }

  /**
   * {@inheritDoc}
   */
  public function getVideoInfo($key) {

    $database = $this->connection->select('video_store', 'vi');
    $database->condition('vi.video_key', $key);
    $results = $database->fields('vi',
      [
        'fid',
        'uid',
        'upload_date',
        'status',
        'description',
      ]
      );
    $results = $results->execute()->fetchAll(\PDO::FETCH_ASSOC);

    return $results;
  }

  /**
   * {@inheritDoc}
   */
  public function getVideoInfoByFid($fid) {

    $database = $this->connection->select('video_store', 'vi');
    $database->condition('vi.fid', $fid);
    $results = $database->fields('vi',
      [
        'fid',
        'uid',
        'upload_date',
        'status',
        'description',
      ]
      );
    $results = $results->execute()->fetchAll(\PDO::FETCH_ASSOC);

    return $results;
  }

  /**
   * {@inheritDoc}
   */
  public function getVideosInfo($uid = NULL) {
    $query = $this->connection->select('video_store', 'vs');
    if ($uid) {
      $uid = $this->checkUser($uid);
    }

    if (is_numeric($uid[0]['uid'] ?? "")) {
      $query->condition('vs.uid', $uid[0]['uid']);
    }

    $query->join('file_managed', 'fm', 'fm.fid = vs.fid');
    $query->join('users_field_data', 'ufd', 'ufd.uid = vs.uid');
    $query->addField('ufd', 'name');
    $query->addField('vs', 'description');
    $query->addField('vs', 'video_key');
    $query->addField('fm', 'filemime');
    $query->addField('fm', 'filename');
    $query->addField('vs', 'upload_date');
    $results = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
    return $results;
  }

  /**
   * {@inheritDoc}
   */
  public function updateVideos($description, $fid, $status) {
    $query = $this->connection->update('video_store');
    $query->condition('fid', [(string) $fid]);
    $query->fields([
      'description' => (string) $description,
      'status' => $status,
    ]);
    $query->execute();

  }

  /**
   * {@inheritDoc}
   */
  public function downloadVideo($identifier, $type = 'fid') {

    // In case a "video_id" is sent, use it to find the needed "fid".
    if ($type === 'video_id') {
      $video = $this->getVideoInfo($identifier);
      if ($video == []) {
        return new RedirectResponse('/');
      }
      $identifier = $video[0]['fid'];
    }
    /** @var \Drupal\file\Entity\File $file */
    $file = $this->fileStorage->load($identifier);
    // Get needed file information.
    $fileName = $file->getFilename();
    $fileUri = $file->getFileUri();
    // Sets HTTP response for a file.
    $baseFileName = $this->getBaseFileName($fileName);
    $response = new BinaryFileResponse($fileUri);
    $response->setContentDisposition('attachment', $baseFileName);
    /** @var \Symfony\Component\HttpFoundation\BinaryFileResponse $response */
    return $response;
  }

  /**
   * {@inheritDoc}
   */
  public function getBaseFileName($fileName) {
    // Find basic string information.
    $extPosition = strrpos($fileName, '.');
    $nameLength = strlen($fileName);
    $extLength = -1 * abs($nameLength - $extPosition);

    // Obtain versions of the string.
    $nameNoExt = substr($fileName, 0, $extPosition);
    $extension = substr($fileName, $extLength, $extPosition);

    // Remove internal ID.
    $idPosition = strrpos($nameNoExt, '_');

    // Validate if it has an ID or not. And return the base name accordingly.
    if ($idPosition === FALSE) {
      return $nameNoExt . $extension;
    }
    $basicName = substr($nameNoExt, 0, $idPosition);

    // Return the base fileName with the extension.
    $fileName = $basicName . $extension;
    return $fileName;
  }

  /**
   * {@inheritDoc}
   */
  public function deleteVideo($fid) {
    /** @var \Drupal\file\Entity\File $file */
    $file = $this->fileStorage->load($fid);
    $file->delete();
    $db = Database::getConnection();
    $query = $db->delete('video_store');
    $query->condition('fid', $fid, '=');
    $query->execute();
  }

  /**
   * {@inheritDoc}
   */
  public function import($forceUpdate = FALSE, $extensions = 'mp4') {
    $extensions = explode(",", $extensions);
    $filemimes = [];

    // Get the files in file managed.
    $query = $this->connection->select('file_managed', 'fm');
    $orGroup1 = $query->orConditionGroup();
    foreach ($extensions as $extension) {
      $filemimes = 'video/' . str_replace(' ', '', $extension);
      $orGroup1->condition('filemime', $filemimes);

    }
    $query->condition($orGroup1);

    $query->fields('fm', ['uid', 'fid', 'created', 'uri']);
    $results = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
    $final = NULL;
    // Create a new array with the final information.
    foreach ($results as $result) {
      // Creates a random number ancd checks it.
      $random = new Random();
      $key = $random->name();
      $keyuse = $this->getVideoInfo($key);

      while ($keyuse != []) {
        $key = $random->name();
        $keyuse = $this->getVideoInfo($key);
      }
      // Skip videos already in db.
      $now = $this->getVideoInfoByFid($result['fid']);

      $privacyState = explode(":", $result['uri'] ?? "")[0];
      $privacy = 0;
      if ($privacyState == 'private') {
        $privacy = 1;
      }

      if (!$now) {
        $final[] = [
          'description' => '.',
          'video_key' => $key,
          'fid' => $result['fid'],
          'uid' => $result['uid'],
          'status' => $privacy,
          'upload_date' => $result['created'],
        ];
      }
      if ($forceUpdate) {
        $final[] = [
          'status' => $privacy,
          'fid' => $result['fid'],
        ];
      }

    }

    if (!$final) {
      return NULL;
    }

    // Insert the information.
    $database = $this->connection->insert('video_store');
    if ($forceUpdate) {
      foreach ($final as $updates) {
        $database = $this->connection->update('video_store');
        $database->condition('fid', $updates['fid']);
        $database->fields(['status' => $updates['status']]);
        $database->execute();
      }

      return TRUE;
    }

    $database->fields(
      [
        'description',
        'video_key',
        'fid',
        'uid',
        'status',
        'upload_date',
      ]);

    foreach ($final as $record) {
      $database->values($record);
    }
    $database->execute();

    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function checkUser($user) {
    $db = $this->connection;
    // Query for videos which contain the searched for value.
    $query = $db->select('users_field_data', 'ufd');
    $query->fields('ufd', ['uid']);
    if (is_numeric($user)) {
      $query->condition('ufd.uid', $user, '=');

    }
    if (is_string($user) && !is_numeric($user)) {
      $query->condition('ufd.name', $user, '=');
    }

    $videos = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

    return $videos;

  }

}

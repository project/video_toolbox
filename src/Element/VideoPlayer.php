<?php

namespace Drupal\video_toolbox\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * Provides a Video render element.
 *
 * Grants the use of an element that renders a video with a custom video library
 * if specified. It can make use of these attributes:
 * - #src             src attribute.
 * - #controls        controls <video> attribute.
 * - #width           width attribute.
 * - #height          height attribute.
 * - #allow           allow <video> attribute.
 * - #autoplay        autoplay <video> attribute.
 * - #loading         loading <video> attribute.
 * - #loop            loop <video> attribute.
 * - #preview         specifies if element should be a preview (Boolean).
 *
 * @RenderElement("video_player")
 */
class VideoPlayer extends RenderElement {

  /**
   * Default render element information.
   *
   * @var array
   */
  const VIDEO_RENDER_INFO = [
    '#src' => NULL,
    '#controls' => TRUE,
    '#width' => 400,
    '#height' => 300,
    '#allow' => 'fullscreen',
    '#autoplay' => FALSE,
    '#loading' => 'eager',
    '#loop' => FALSE,
    '#preview' => FALSE,
    '#attributes' => [],
    '#theme' => 'video_player',
  ];

  /**
   * Main class for video player.
   *
   * @var string
   */
  const VIDEO_CLASS = 'video-frame';

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = static::class;
    $preRender = [
      '#pre_render' => [
        [$class, 'preRenderVideoPlayer'],
      ],
    ];

    $info = array_merge(static::VIDEO_RENDER_INFO, $preRender);
    return $info;
  }

  /**
   * Pre-render callback: Renders a generic HTML tag with attributes.
   *
   * @param array $element
   *   Render element before setting up attributes.
   *
   * @return array
   *   Render element with default and specified attributes.
   */
  public static function preRenderVideoPlayer(array $element) {
    // Set the attributes that were not specified.
    // Width.
    $element['#attributes']['width'] = $element['#width'];

    // Height.
    $element['#attributes']['height'] = $element['#height'];

    // Source.
    $element['#attributes']['src'] = $element['#src'];

    // Controls.
    $element['#attributes']['controls'] = $element['#controls'];

    // Allow.
    $element['#attributes']['allow'] = $element['#allow'];

    // Autoplay.
    $element['#attributes']['autoplay'] = $element['#autoplay'];

    // Loading.
    $element['#attributes']['loading'] = $element['#loading'];

    // Loop.
    $element['#attributes']['loop'] = $element['#loop'];

    // If there are no classes set, set the video-frame class, if there are,
    // then just append the class.
    if (!isset($element['#attributes']['class'])) {
      $element['#attributes']['class'] = static::VIDEO_CLASS;
    }
    else {
      $element['#attributes']['class'] .= ' ' . static::VIDEO_CLASS;
    }

    // Unset unneded indexes.
    unset($element['#width']);
    unset($element['#height']);
    unset($element['#src']);
    unset($element['#allow']);
    unset($element['#autoplay']);
    unset($element['#loading']);
    unset($element['#loop']);
    return $element;
  }

}

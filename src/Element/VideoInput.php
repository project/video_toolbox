<?php

namespace Drupal\video_toolbox\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Textfield;

/**
 * Provides a form element for searching existing videos.
 *
 * Extends the Textfield FormElement, it has a video validation and autocomplete
 * function by default.
 *
 * Properties:
 * - #maxlength: Maximum number of characters of input allowed.
 * - #size: The size of the input element in characters.
 * - #autocomplete_route_name: A route to be used as callback URL by the
 *   autocomplete JavaScript library.
 * - #autocomplete_route_parameters: An array of parameters to be used in
 *   conjunction with the route name.
 * - #pattern: A string for the native HTML5 pattern attribute.
 *
 * Usage example:
 * @code
 * $form['form_id'] = [
 *   '#type' => 'video_input',
 *   '#title' => $this->t('Title'),
 *   '#default_value' => $node->title,
 *   '#maxlength' => 128,
 *   '#pattern' => 'some-prefix-[a-z]+',
 *   '#required' => TRUE,
 * ];
 * @endcode
 *
 * @see \Drupal\Core\Render\Element\Textfield
 *
 * @FormElement("video_input")
 */
class VideoInput extends Textfield {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $newInfo = [
      '#autocomplete_route_name' => 'video_toolbox.autocomplete_videos',
      '#element_validate' => [
        [static::class, 'validate'],
      ],
    ];
    return array_merge($info, $newInfo);
  }

  /**
   * Validates video information.
   */
  public static function validate($element, FormStateInterface $form_state) {
    $value = $element['#value'];
    if (!$value) {
      return;
    }
    $videogetter = \Drupal::service('video.get_service');
    $videos = $videogetter->getVideoInfo($value);

    if (!$videos) {
      $form_state->setError($element, t('Your video ID is not valid'));
    }
  }

}

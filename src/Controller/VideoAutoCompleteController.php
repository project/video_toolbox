<?php

namespace Drupal\video_toolbox\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Database\Connection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\video_toolbox\VideoHandler;

/**
 * Controller in charge of returning a JSON request for video Autocompletion.
 */
class VideoAutoCompleteController extends ControllerBase {

  /**
   * The request.
   *
   * @var Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The videoHandler service helps with video management.
   *
   * @var \Drupal\video_toolbox\VideoHandler
   */
  protected $videoHandler;

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The VideoAutoCompleteController constructor.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\video_toolbox\VideoHandler $video_handler
   *   The videoHandler service helps with video management.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(
    Request $request,
    VideoHandler $video_handler,
    Connection $database
  ) {
    $this->request = $request;
    $this->videoHandler = $video_handler;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('video.get_service'),
      $container->get('database'),
    );
  }

  /**
   * Handles the information regarding elegible videos.
   */
  public function handleVideoAutoComplete(Request $request) {
    // Redirect in case of direct access to the route.
    $referer = $request->headers->get('referer');
    if ($referer == NULL) {
      return new RedirectResponse('/');
    }

    $input = $request->query->get('q');
    $results = [];

    $db = $this->database;
    // Query for videos which contain the searched for value.
    $query = $db->select('video_store', 'vs');
    $query->join('file_managed', 'fm', 'fm.fid = vs.fid');
    $query->addField('fm', 'filename');
    $query->addField('vs', 'video_key');
    $query->addField('vs', 'status');
    $query->condition('filename', '%' . $input . '%', 'LIKE');
    $videos = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

    // Build Entity and label for field.
    foreach ($videos as $video) {
      $status = '(public)';

      if ($video['status'] == 1) {
        $status = '(private)';
      }
      $label = [
        $video['filename'] . " " . $status,
      ];
      $results[] = [
        'value' => $video['video_key'],
        'label' => $label,
      ];
    }

    // Return JSON with the autocomplete information.
    return new JsonResponse($results);
  }

  /**
   * Handles the information regarding elegible videos.
   */
  public function handleUserAutocomplete(Request $request) {
    // Redirect in case of direct access to the route.
    $referer = $request->headers->get('referer');
    if ($referer == NULL) {
      return new RedirectResponse('/');
    }

    $input = $request->query->get('q');
    $results = [];

    $db = $this->database;
    // Query for videos which contain the searched for value.
    $query = $db->select('users_field_data', 'ufd');
    $query->fields('ufd', ['name', 'uid']);
    $query->condition('name', '%' . $input . '%', 'LIKE');
    $videos = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

    // Build Entity and label for field.
    foreach ($videos as $video) {
      $label = [
        $video['name'],
      ];
      $results[] = [
        'value' => $label,
        'label' => $label,
      ];
    }

    // Return JSON with the autocomplete information.
    return new JsonResponse($results);
  }

}

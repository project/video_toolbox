<?php

namespace Drupal\video_toolbox\Controller;

use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Url;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\video_toolbox\Traits\VideoFormDependencyInjectionTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Controller for videos list report.
 */
class VideoToolBoxReportController extends ControllerBase {

  use VideoFormDependencyInjectionTrait;


  /**
   * Type of the video identifier.
   */
  const VIDEO_IDENTIFIER_TYPE = 'video_id';

  /**
   * Creates the report page.
   *
   * @return array
   *   Render array for report output.
   */
  public function report(Request $request) {

    $roles = $this->account->getRoles();

    $uid = $this->account->id();

    $permissions = $this->account->hasPermission('upload_content_videos');

    $content = [];

    $user = $request->query->all();
    // Check for require upload_content_videos permission.
    if ($permissions) {
      $content['add'] = [
        '#type' => 'link',
        '#title' => $this->t('Add video'),
        '#attributes' => [
          'class' => ['button button--primary'],
        ],
        '#url' => Url::fromRoute('video_toolbox.form'),
      ];
    }

    $content['message'] = [
      '#markup' => '<br>' . $this->t('Below is a list of all uploaded videos including user, video description, video format, video link and video upload date.'),
    ];

    $headers = [
      $this->t('User'),
      $this->t('Description'),
      $this->t('Link'),
      $this->t('Uploaded date'),
      $this->t('Format'),
      $this->t('Download'),
    ];
    // Checks for user.
    if (!$user) {
      $user['user'] = NULL;
    }
    // Search if user is an admin.
    if (array_search("administrator", $roles)) {
      $uid = NULL ?? $user['user'];
      $form = $this->formBuilder()->getForm('\Drupal\video_toolbox\Form\VideoSearchForm');
      $content['search'] = $form;
    }
    // If admin is not found then user uid.
    if (!array_search("administrator", $roles)) {
      $user['user'] = $uid;
    }
    // If user.
    if ($user) {
      $uid = $user['user'];
    }
    // If user is not found then redirect.
    if (is_string($uid) && $uid == "") {
      return new RedirectResponse("/");
    }

    $videos = $this->videoHandler->getVideosInfo($uid);
    $rows = [];

    foreach ($videos as $entry) {
      // Cuts the description short in case it is too long for the table.
      if (strlen($entry['description']) > 20) {
        $entry['description'] = substr($entry['description'], 0, 20) . '...';
      }
      // Obtains URLs from given routes.
      $url = Url::fromRoute('video_toolbox.videos_url', ['video_id' => $entry['video_key']])->toString();
      $urlDownload = Url::fromRoute('video_toolbox.download', [
        'identifier' => $entry['video_key'],
        'type' => self::VIDEO_IDENTIFIER_TYPE,
      ])->toString();
      $rows[] = [
        $entry['name'],
        $entry['description'],
        new FormattableMarkup('<a href=":link">@name</a>',
        [
          ':link' => $url,
          '@name' => $entry['filename'],
        ]),
        date("d/m/Y", $entry['upload_date']),
        $entry['filemime'],
        new TranslatableMarkup('<a class="button button--primary" href=":link">@download</a>',
        [
          ':link' => $urlDownload,
          '@download' => 'Download',
        ]
        ),
      ];
    }
    $content['table'] = [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => $this->t('No entries available'),
    ];
    // Don't cache this page.
    $content['#cache']['max-age'] = 0;
    return $content;
  }

}

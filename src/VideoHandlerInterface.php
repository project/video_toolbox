<?php

namespace Drupal\video_toolbox;

use Drupal\Core\Database\Connection;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Interface for videoHandlerInterface.
 */
interface VideoHandlerInterface {

  /**
   * Construct for dependency injection.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The current user.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The dMessenger Serivce.
   */
  public function __construct(
    Connection $connection,
    EntityTypeManagerInterface $entity_type_manager,
    MessengerInterface $messenger
  );

  /**
   * Saves the video information in the database.
   *
   * @param string $description
   *   The description of the video.
   * @param string $video
   *   Video identifier (video_id).
   * @param string||int $user_id
   *   The user id.
   * @param string||int $fid
   *   The file id to locate the video.
   * @param int $status
   *   Status of the video.
   * @param string||int $time
   *   Time of creation.
   */
  public function saveVideoInfo($description, $video, $user_id, $fid, $status, $time = NULL);

  /**
   * Gets the video information in the database by video id.
   *
   * @param string $key
   *   The video key or ID to locate it.
   *
   * @return array
   *   The video information.
   */
  public function getVideoInfo($key);

  /**
   * Gets the video information in the database by file id.
   *
   * @param string $fid
   *   The video file id locate it.
   *
   * @return array
   *   The video information.
   */
  public function getVideoInfoByFid($fid);

  /**
   * Get the list of all the videos in the database.
   *
   * @param string||int||null $uid
   *   User Id.
   *
   * @return array
   *   List of videos and their information.
   */
  public function getVideosInfo($uid);

  /**
   * Update the information in the database of an specific video.
   *
   * @param string $description
   *   The new description to update.
   * @param string $fid
   *   The video file id locate it.
   * @param string $status
   *   The video file id locate it.
   */
  public function updateVideos($description, $fid, $status);

  /**
   * Download a specific video with given identifier.
   *
   * @param string||int $identifier
   *   Video identifier (fid or video_id).
   * @param string $type
   *   Specifies the given identifier for the video. Possible identifiers are:
   *    - **"fid"** (File ID), internal ID used by File object.
   *    - **"video_id"** (Video ID), the random ID (Key) of each video.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   *   HTTP Response.
   */
  public function downloadVideo($identifier, $type = 'fid');

  /**
   * Remove unnecessary parts of the file name (Like the ID).
   *
   * @param string $fileName
   *   The file name to be sanitized.
   *
   * @return string
   *   Sanitized file name.
   */
  public function getBaseFileName($fileName);

  /**
   * Delete  videos from DB and storage by fid.
   *
   * @param string $fid
   *   The video file id locate it.
   */
  public function deleteVideo($fid);

  /**
   * Import all the videos in file managed to the database.
   */
  public function import($forcerUpdate = FALSE, $extensions = 'mp4');

  /**
   * Import all the videos in file managed to the database.
   */
  public function checkUser($user);

}

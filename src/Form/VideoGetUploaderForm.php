<?php

namespace Drupal\video_toolbox\Form;

use Drupal\Component\Utility\Random;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Form for uploading videos.
 */
class VideoGetUploaderForm extends VideoFormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'video_getter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $random = new Random();
    $key = $random->name();
    // Config.
    $config = $this->config(self::VIDEO_TOOLBOX_SETTINGS);
    // User ID.
    $uid = $this->account->id();

    $form['video_desc'] = [
      '#type' => 'textarea',
      '#title' => "Description",
      '#required' => TRUE,
    ];

    $form['key'] = [
      '#type' => 'hidden',
      '#value' => $key,
    ];

    $form['file'] = [
      '#type' => 'managed_file',
      '#title' => 'upload video',
      '#upload_location' => $config->get('folder_pub') ?? 'public://video_saving/',
      '#upload_validators' => [
        'file_validate_extensions' => [str_replace(',', ' ', $config->get('allowed_extensions') ?? 'mp4')],
      ],
      '#required' => TRUE,
    ];

    $form['privacy'] = [
      '#type' => 'radios',
      '#title' => $this
        ->t('Visibility'),
      '#default_value' => 0,
      '#options' => [
        0 => $this
          ->t('Public'),
        1 => $this
          ->t('Private'),
      ],
    ];

    $form['uid'] = [
      '#type' => 'hidden',
      '#value' => $uid,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $key = $form_state->getValue('key');
    $check = $this->videoHandler->getVideoInfo($key);
    if ($check != []) {
      $form_state->setErrorByName('key', $this->t('This key is being used'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(self::VIDEO_TOOLBOX_SETTINGS);

    $videogetter = $this->videoHandler;
    $type = $form_state->getValue('video_desc');
    $video = $form_state->getValue('file', 0);
    if (isset($video[0]) && !empty($video[0])) {
      /** @var \Drupal\file\Entity\File $file */
      $file = $this->fileStorage->load($video[0]);
      $file->setPermanent();
      $file->save();
      $fid = $file->id();
    }
    if ($form_state->getValue('privacy') == 1) {
      /** @var \Drupal\file\FileRepositoryInterface $fileRepository */
      $this->fileRepository->move($file, $config->get('folder_priv') ?? 'private://video_hidden_saving/');
    }
    $key = $form_state->getValue('key');
    $user_id = $form_state->getValue('uid');
    $status = $form_state->getValue('privacy');
    $videogetter->saveVideoInfo($type, $key, $user_id, $fid, $status);
    $this->messenger()->addStatus($this->t('Video successfully saved.'));

    return new RedirectResponse('reports/uploaded_videos');
  }

}

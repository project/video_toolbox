<?php

namespace Drupal\video_toolbox\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\video_toolbox\VideoHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration Form for the video_toolbox module.
 */
class VideoToolBoxConfigForm extends ConfigFormBase {

  /**
   * Video Handler Service.
   *
   * @var \Drupal\video_toolbox\VideoHandlerInterface
   */
  protected $videoHandler;

  /**
   * Constructor to initialize Services.
   */
  public function __construct(VideoHandlerInterface $videoHandler) {
    $this->videoHandler = $videoHandler;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('video.get_service'),
      $container->get('messenger'),
    );
  }

  /**
   * Config settings for video_toolbox module.
   *
   * @var string
   */
  const VIDEO_TOOLBOX_SETTINGS = 'video_toolbox.settings';

  /**
   * Extensions.
   *
   * @var array
   */
  const EXT = [
    "avi",
    "flv",
    "mkv",
    "mov",
    "mp4",
    "webm",
  ];

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'video_toolbox_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      self::VIDEO_TOOLBOX_SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::VIDEO_TOOLBOX_SETTINGS);

    $form['forceUpdate'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Force import'),
      '#description' => $this->t('This will delete all your videos and reimport them again, but for now it only updates FID and visibility in case of any issue with those. ej change video to another folder'),
    ];

    $form['import'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import all videos'),
      '#description' => $this->t("Make sure to fill 'allowed extensions' and save it, this will allowed to import videos in the formats that you entered."),
      '#attributes' => [
        'class' => ['button button--primary'],
      ],
      '#submit' => ['::import'],
    ];

    $form['allowed_extensions'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Allowed extensions'),
      '#description' => $this->t('This is for the video extensions that you can allowed, separate them witha comma'),
      '#default_value' => $config->get('allowed_extensions') ?? 'mp4',
      '#required' => 'true',
    ];

    $form['public'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Public video folder'),
      '#description' => $this->t('Make sure that the structure is public://{custom folder}/, ex "public://epic/"'),
      '#default_value' => $config->get('folder_pub') ?? 'public://video_saving/',
      '#required' => 'true',
    ];

    $form['private'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private video folder'),
      '#description' => $this->t('Make sure that the structure is private://{custom folder}/, ex "private://epic/", for private paths please setup a path to <br> in /admin/config/media/file-system and create a folder inside the private path with the name of your {custom folder}'),
      '#default_value' => $config->get('folder_priv') ?? 'private://video_hidden_saving/',
      '#required' => 'true',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // Check for extensions.
    $ext = explode(',', preg_replace('/[ ]+/', '', $form_state->getValue('allowed_extensions')));

    if (count(array_intersect($ext, self::EXT)) != count($ext)) {
      $form_state->setErrorByName('allowed_extensions', $this->t('Invalid extension!'));
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save configuration on submit.
    $config = $this->config(self::VIDEO_TOOLBOX_SETTINGS);
    $config
      ->set('allowed_extensions', $form_state->getValue('allowed_extensions'))
      ->set('folder_pub', $form_state->getValue('public'))
      ->set('folder_priv', $form_state->getValue('private'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Import.
   */
  public function import(array &$form, FormStateInterface $form_state) {
    $force = FALSE;
    if ($form_state->getValue('forceUpdate') == 1) {
      $force = TRUE;
    }
    $import = $this->videoHandler->import($force, $form_state->getValue('allowed_extensions'));

    if ($import) {
      $this->messenger()->addMessage($this->t('Videos imported successfully'));
    }
    else {
      $this->messenger()->addWarning($this->t('Nothing to import'));
    }

  }

}

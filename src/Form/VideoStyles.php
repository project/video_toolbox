<?php

namespace Drupal\video_toolbox\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the Example add and edit forms.
 */
class VideoStyles extends EntityForm {

  /**
   * Constructs an VideoRatios object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'video_toolbox_add';
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\video_toolbox\Entity\VideoStylesEntityInterface $videoRatio */
    $videoRatio = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $videoRatio->label(),
      '#description' => $this->t("Label for the video."),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $videoRatio->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$videoRatio->isNew(),
    ];

    $form['attributes'] = [
      '#type' => 'textfield',
      '#title' => $this
        ->t('Video attributes'),
      '#default_value' => $videoRatio->getVideoAtt(),
    ];

    $form['height'] = [
      '#type' => 'number',
      '#title' => $this
        ->t('Height'),
      '#default_value' => $videoRatio->getHeight() ?? '300',
      '#required' => TRUE,
    ];
    $form['width'] = [
      '#type' => 'number',
      '#title' => $this
        ->t('Width'),
      '#default_value' => $videoRatio->getWidth() ?? '500',
      '#required' => TRUE,
    ];

    // You will need additional form elements for your custom properties.
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $videoRatio = $this->entity;
    $status = $videoRatio->save();

    if ($status === SAVED_NEW) {
      $this->messenger()->addMessage($this->t('The %label Example created.', [
        '%label' => $videoRatio->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label Example updated.', [
        '%label' => $videoRatio->label(),
      ]));
    }

    $form_state->setRedirect('entity.video_toolbox_styles.collection');
  }

  /**
   * Helper function to check whether an Example configuration entity exists.
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('video_toolbox_styles')->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}

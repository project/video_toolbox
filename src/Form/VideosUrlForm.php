<?php

namespace Drupal\video_toolbox\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides a Video toolbox form.
 */
class VideosUrlForm extends VideoFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'video_default_url';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $video_id = NULL) {

    $videogetter = $this->videoHandler;
    $config = $this->config(self::VIDEO_TOOLBOX_SETTINGS);

    $configStyle = $this->config($config->get('video_styles'));
    $videos = $videogetter->getVideoInfo($video_id);
    if ($videos == []) {
      return new RedirectResponse('/');
    }

    $uid = $this->account->id();
    $roles = $this->account->getRoles();
    $permissions = $this->account->hasPermission('view_hidden_content_vt');

    /** @var \Drupal\file\Entity\File $file */
    $file = $this->fileStorage->load($videos[0]['fid']);
    $videourl = $file->createFileUrl();

    $form['video'] = [
      '#type' => 'video_player',
      '#src' => $videourl,
      '#width' => '100%',
      '#height' => 315,
      '#attributes' => [
        'class' => $configStyle->get('attributes') ?? '',
      ],
    ];
    if ($videos[0]['uid'] != $uid && !array_search("administrator", $roles)) {
      if ($videos[0]['status'] == '1' && !$permissions) {
        return new RedirectResponse('/');
      }
      return $form;
    }

    $form['privacy'] = [
      '#type' => 'radios',
      '#title' => $this
        ->t('Visibility'),
      '#default_value' => $videos[0]['status'] ?? 0,
      '#options' => [
        0 => $this
          ->t('Public'),
        1 => $this
          ->t('Private'),
      ],
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#required' => TRUE,
      '#default_value' => $videos[0]['description'],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#validate' => ['::validateFormSave'],
    ];

    $form['download'] = [
      '#type' => 'submit',
      '#value' => $this->t('Download'),
      '#submit' => ['::downloadSubmit'],
      '#validate' => ['::downloadValidate'],
    ];
    $form['delete'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete'),
      '#attributes' => ['onclick' => 'if(!confirm("Delete video?")){return false;}'],
      '#submit' => ['::delete'],
    ];

    $form['fid'] = [
      '#type' => 'hidden',
      '#value' => $videos[0]['fid'],
    ];

    return $form;
  }

  /**
   * Validate Form inputs only for the Save button.
   */
  public function validateFormSave(array &$form, FormStateInterface $form_state) {
    $desc = $form_state->getValue('description');
    if (is_string($desc) && mb_strlen($desc) < 1) {
      $form_state->setErrorByName('description', $this->t('Description should be at least 10 characters.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(self::VIDEO_TOOLBOX_SETTINGS);
    // Get values stored in form state.
    $desc = $form_state->getValue('description');
    $fid = $form_state->getValue('fid');
    $videogetter = $this->videoHandler;

    $privacy = $form_state->getValue('privacy');

    /** @var \Drupal\file\Entity\File $file */
    $file = $this->fileStorage->load($fid);
    $privacyState = explode(":", $file->getFileUri() ?? "")[0];

    if ($privacy == 1 && $privacyState != 'private') {
      /** @var \Drupal\file\FileRepositoryInterface $fileRepository */
      $this->fileRepository->move($file, $config->get('folder_priv') ?? 'private://video_hidden_saving/');
    }
    if ($privacy == 0 && $privacyState != 'public') {
      /** @var \Drupal\file\FileRepositoryInterface $fileRepository */
      $this->fileRepository->move($file, $config->get('folder_pub') ?? 'public://video_saving/');

    }
    $videogetter->updateVideos($desc, $fid, $privacy);
    $this->messenger()->addStatus($this->t('Settings saved'));

  }

  /**
   * Validate Download submission.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function downloadValidate(array &$form, FormStateInterface $form_state) {
    // Used just to override default validation for the time being.
  }

  /**
   * Submit Handler for download button.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function downloadSubmit(array &$form, FormStateInterface $form_state) {
    $videogetter = $this->videoHandler;
    // Get value from form and use downloadVideo method.
    $fid = $form_state->getValue('fid');
    $response = $videogetter->downloadVideo($fid);
    // Will set the response of the form to the response returned.
    $form_state->setResponse($response);
  }

  /**
   * Delete the video.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function delete(array &$form, FormStateInterface $form_state) {
    $videoService = $this->videoHandler;
    $fid = $form_state->getValue('fid');
    $videoService->deleteVideo($fid);
    // Redirect to reports.
    $url = Url::fromRoute('video_toolbox.report');
    $url = $url->toString();
    $response = new RedirectResponse($url);
    $response->send();

  }

}

<?php

namespace Drupal\video_toolbox\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\video_toolbox\VideoHandlerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\file\FileRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a trait for the depency injection for forms.
 */
abstract class VideoFormBase extends FormBase {

  /**
  * Config settings for video_toolbox module.
  *
  * @var string
  */
  const VIDEO_TOOLBOX_SETTINGS = 'video_toolbox.settings';

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * Account Service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * Video Handler Service.
   *
   * @var \Drupal\video_toolbox\VideoHandlerInterface
   */
  protected $videoHandler;

  /**
   * The storage handler class for files.
   *
   * @var \Drupal\file\FileStorage
   */
  protected $fileStorage;

  /**
   * The FileRepository service for moving files.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * Constructor to initialize Services.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The Account Service for user information.
   * @param \Drupal\video_toolbox\VideoHandlerInterface $videoHandler
   *   The Video Handler Service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Manager Service, used for File entities.
   * @param \Drupal\file\FileRepositoryInterface $file_repository
   *   The FileRepository service for moving files.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The FileRepository service for moving files.
   */
  public function __construct(
    AccountInterface $account,
    VideoHandlerInterface $videoHandler,
    EntityTypeManagerInterface $entity_type_manager,
    FileRepositoryInterface $file_repository,
    ConfigFactoryInterface $configFactory
  ) {
    $this->account = $account;
    $this->videoHandler = $videoHandler;
    $this->fileStorage = $entity_type_manager->getStorage('file');
    $this->fileRepository = $file_repository;
    $this->entityManager = $entity_type_manager;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('current_user'),
      $container->get('video.get_service'),
      $container->get('entity_type.manager'),
      $container->get('file.repository'),
      $container->get('config.factory'),
    );
  }

}

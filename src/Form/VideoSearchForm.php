<?php

namespace Drupal\video_toolbox\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Form for searching.
 */
class VideoSearchForm extends VideoFormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'search_form_video';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['query'] = [
      '#type' => 'textfield',
      '#placeholder' => $this->t('Search'),
      '#required' => TRUE,
      '#autocomplete_route_name' => 'video_toolbox.autocomplete_users',
      '#ajax' => [
        'callback' => [$this, 'check'],
        'event' => 'change',
        'wrapper' => 'submit_search',
        'disable-refocus' => FALSE,
      ],
    ];
    $form['search'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      '#attributes' => [
        'class' => ['button button--primary'],
        'disabled' => TRUE,
        'id' => 'submit_search',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function check(array &$form, FormStateInterface $form_state) {
    $form['search']['#attributes']['disabled'] = TRUE;
    $values = $form_state->getValues();

    $validate = $this->videoHandler->checkUser($values['query']);
    if ($validate != []) {
      $form['search']['#attributes']['disabled'] = FALSE;
    }
    return $form['search'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $path = Url::fromRoute('video_toolbox.report',
    ['user' => $values['query']]);
    $response = new RedirectResponse($path->toString());
    $response->send();

  }

}

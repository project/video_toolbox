<?php

namespace Drupal\video_toolbox\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\video_toolbox\VideoHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration Form for the video_toolbox module.
 */
class VideoToolBoxVideoPlayerConfigForm extends ConfigFormBase {

  /**
   * The Options.
   *
   * @var array
   */
  const OPTIONS = [
    'btn_play' => '<i class="fa fa-play"></i>',
    'btn_fwrdback' => '<i class="fa fa-fast-backward"></i> <i class="fa fa-fast-forward"></i>',
    'btn_fullscreen' => '<i class="fa fa-expand"></i>',
    'btn_speed' => '1X',
    'btn_sound' => '<i class="fas fa-volume-up"></i>',
  ];

  /**
   * Video Handler Service.
   *
   * @var \Drupal\video_toolbox\VideoHandlerInterface
   */
  protected $videoHandler;

  /**
   * Constructor to initialize Services.
   */
  public function __construct(VideoHandlerInterface $videoHandler) {
    $this->videoHandler = $videoHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('video.get_service'),
      $container->get('messenger'),
    );
  }

  /**
   * Config settings for video_toolbox module.
   *
   * @var string
   */
  const VIDEO_TOOLBOX_SETTINGS = 'video_toolbox.settings';

  /**
   * Default configurations for the Video Player, colors are hexadecimal.
   *
   * @var string
   */
  const DEFAULT_PLAYER_SETTINGS = [
    'default_control_color' => 0,
    'control_color' => '#ffffff',
    'bar_color' => '#010141',
    'progress_color' => '#cccccc',
    'button_color' => '#ffffff',
    'active_controls' => [
      'btn_play' => TRUE,
      'btn_fwrdback' => TRUE,
      'control_bar' => TRUE,
      'btn_fullscreen' => TRUE,
      'btn_speed' => TRUE,
      'btn_sound' => TRUE,
    ],
  ];

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'video_toolbox_configuration_video_player_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      self::VIDEO_TOOLBOX_SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::VIDEO_TOOLBOX_SETTINGS);
    $controlColor = $config->get('video_player.control_color');
    $activeControls = array_keys(array_filter($config->get('video_player.active_controls') ?? self::DEFAULT_PLAYER_SETTINGS['active_controls']));

    // In case the confiugration is set to "none", change this variable
    // to the default color white.
    if ($controlColor === 'none') {
      $controlColor = self::DEFAULT_PLAYER_SETTINGS['control_color'];
    }

    $configNames = $this->configFactory->listAll('video_toolbox.video_toolbox_styles');
    $options[] = $this->t('None');
    foreach ($configNames as $configEntity) {
      $configObject = $this->config($configEntity);
      $options[$configEntity] = $configObject->get('id');
    }

    $form['styles'] = [
      '#type' => 'select',
      '#title' => $this
        ->t('Select style'),
      '#default_value' => $config->get('video_styles'),
      '#options' => $options,
    ];

    $form['enable_custom_player'] = [
      '#type' => 'radios',
      '#title' => $this->t('Custom Player'),
      '#default_value' => $config->get('video_player.enable_custom_player'),
      '#options' => [
        1 => $this->t('Enable'),
        0 => $this->t('Disable'),
      ],
    ];

    $form['video_player_config'] = [
      '#type' => 'fieldgroup',
      '#title' => $this->t('Customize Player'),
      '#states' => [
        'visible' => [
          ':input[name="enable_custom_player"]' => ['value' => 1],
        ],
      ],
    ];

    $form['video_player_config']['set_default_btn'] = [
      '#prefix' => '<hr>',
      '#type' => 'submit',
      '#title' => $this->t('Set default configuration for Video player'),
      '#value' => $this->t('Reset style configuration to the default value'),
      '#attributes' => [
        'class' => ['button button--primary'],
      ],
      '#submit' => ['::setDefaultVideoPlayer'],
    ];
    $options = self::OPTIONS;
    $options['control_bar'] = $this->t('Timeline Bar');
    $form['video_player_config']['active_controls'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Choose Video player functions'),
      '#options' => $options,
      '#default_value' => $activeControls,
    ];

    $form['video_player_config']['default_control_color'] = [
      '#type' => 'radios',
      '#title' => $this->t('Choose default for controls background'),
      '#default_value' => $config->get('video_player.default_control_color'),
      '#options' => [
        0 => $this->t('Transparency'),
        1 => $this->t('Choose Color'),
      ],
    ];

    $form['video_player_config']['control_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Video Control Color'),
      '#default_value' => $controlColor,
      '#states' => [
        'visible' => [
          ':input[name="default_control_color"]' => ['value' => 1],
        ],
      ],
    ];

    $form['video_player_config']['bar_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Video Bar Color'),
      '#default_value' => $config->get('video_player.bar_color'),
    ];

    $form['video_player_config']['progress_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Video Progress Bar Color'),
      '#default_value' => $config->get('video_player.progress_color'),
    ];

    $form['video_player_config']['button_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Video Buttons Color'),
      '#default_value' => $config->get('video_player.button_color'),
    ];

    $form['video_player_config']['preview_div'] = [
      '#type' => 'video_player',
      '#title' => $this->t('Preview of Video Player Style'),
      '#width' => '500px',
      '#height' => '375px',
      '#preview' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save configuration on submit.
    $controlDefault = $form_state->getValue('default_control_color');
    $config = $this->config(self::VIDEO_TOOLBOX_SETTINGS);
    // Transform all values to boolean.
    $activeControls = $form_state->getValue('active_controls');
    foreach ($activeControls as $key => $value) {
      $activeControls[$key] = (bool) $value;
    }
    // Set configuration values.
    $config
      ->set('video_player.enable_custom_player', $form_state->getValue('enable_custom_player'))
      ->set('video_player.bar_color', $form_state->getValue('bar_color'))
      ->set('video_player.progress_color', $form_state->getValue('progress_color'))
      ->set('video_player.button_color', $form_state->getValue('button_color'))
      ->set('video_styles', $form_state->getValue('styles'))
      ->set('video_player.active_controls', $activeControls);
    // Depending on the Transparency or Color choice.
    if ($controlDefault == 0) {
      $config->set('video_player.control_color', 'none');
      $config->set('video_player.default_control_color', 0);
    }
    else {
      $config->set('video_player.control_color', $form_state->getValue('control_color'));
      $config->set('video_player.default_control_color', 1);
    }
    $config->save();
    drupal_flush_all_caches();
    parent::submitForm($form, $form_state);
  }

  /**
   * Set the default values for the Video Player settings.
   */
  public function setDefaultVideoPlayer(array &$form, FormStateInterface $form_state) {
    $form_state->setValue('default_control_color', self::DEFAULT_PLAYER_SETTINGS['default_control_color']);
    $form_state->setValue('control_color', self::DEFAULT_PLAYER_SETTINGS['control_color']);
    $form_state->setValue('bar_color', self::DEFAULT_PLAYER_SETTINGS['bar_color']);
    $form_state->setValue('progress_color', self::DEFAULT_PLAYER_SETTINGS['progress_color']);
    $form_state->setValue('button_color', self::DEFAULT_PLAYER_SETTINGS['button_color']);
    $form_state->setValue('active_controls', self::DEFAULT_PLAYER_SETTINGS['active_controls']);
    $this->submitForm($form, $form_state);
  }

}

<?php

namespace Drupal\video_toolbox\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'video_toolbox_item' widget.
 *
 * @FieldWidget(
 *   id = "video_t_widget",
 *   label = @Translation("Video ID"),
 *   field_types = {
 *     "video_toolbox_item"
 *   }
 * )
 */
class VideoToolWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $value = $items[$delta]->value ?? '';
    $element += [
      '#type' => 'video_input',
      '#title' => $this->t('Video ID'),
      '#description' => $this->t('Insert the video ID'),
      '#default_value' => $value,
    ];

    return ['value' => $element];
  }

}

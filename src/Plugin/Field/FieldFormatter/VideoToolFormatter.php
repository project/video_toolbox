<?php

namespace Drupal\video_toolbox\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\video_toolbox\VideoHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'video_toolbox_item' formatter.
 *
 * @FieldFormatter(
 *   id = "video_t_formatter",
 *   label = @Translation("video_toolbox_formatter"),
 *   field_types = {
 *     "video_toolbox_item"
 *   }
 * )
 */
class VideoToolFormatter extends FormatterBase {

  /**
   * Video Handler Service.
   *
   * @var \Drupal\video_toolbox\VideoHandlerInterface
   */
  protected $videoHandler;

  /**
   * The storage handler class for files.
   *
   * @var \Drupal\file\FileStorage
   */
  protected $fileStorage;

  /**
   * Constructs a StringFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\video_toolbox\VideoHandlerInterface $videoHandler
   *   The Video Handler Service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, VideoHandlerInterface $videoHandler, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->videoHandler = $videoHandler;
    $this->fileStorage = $entity_type_manager->getStorage('file');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('video.get_service'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Displays the random string.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {

      $videogetter = $this->videoHandler;
      $videos = $videogetter->getVideoInfo($item->value);
      /** @var \Drupal\file\Entity\File $file */
      $file = $this->fileStorage->load($videos[0]['fid']);
      $videourl = $file->createFileUrl();
      // Render each element as markup.
      $element[$delta] = [
        '#type' => 'video_player',
        '#src' => $videourl,
        '#width' => 420,
        '#height' => 300,
      ];
    }

    return $element;
  }

}

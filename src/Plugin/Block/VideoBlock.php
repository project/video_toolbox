<?php

namespace Drupal\video_toolbox\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\video_toolbox\VideoHandlerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'video' Block.
 *
 * @Block(
 *   id = "video_block",
 *   admin_label = @Translation("Video Block"),
 *   category = @Translation("video"),
 * )
 */
class VideoBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Video Handler Service.
   *
   * @var \Drupal\video_toolbox\VideoHandlerInterface
   */
  protected $videoHandler;

  /**
   * The storage handler class for files.
   *
   * @var \Drupal\file\FileStorage
   */
  protected $fileStorage;

  /**
   * Constructor to initialize Services.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param mixed $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\video_toolbox\VideoHandlerInterface $videoHandler
   *   The Video Handler Service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Manager Service, used for File entities.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    VideoHandlerInterface $videoHandler,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->videoHandler = $videoHandler;
    $this->fileStorage = $entity_type_manager->getStorage('file');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('video.get_service'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->getConfiguration();

    $videogetter = $this->videoHandler;
    $videos = $videogetter->getVideoInfo($config['video']['src']);
    /** @var \Drupal\file\Entity\File $file */
    $file = $this->fileStorage->load($videos[0]['fid']);
    if ($file) {
      $videourl = $file->createFileUrl();
    }

    $block['video_bl'] = [
      '#type' => 'video_player',
      '#src' => $videourl,
      '#autoplay' => TRUE,
      '#width' => $config['video']['width'],
      '#height' => $config['video']['height'],
      '#loop' => TRUE,
      '#attributes' => [
        'class' => $config['video']['att'] ?? NULL,
      ],

    ];
    if ($config['video']['controls']) {

      $block['video_bl']['#controls'] = FALSE;
    }
    return $block;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form['video_id'] = [
      '#type' => 'video_input',
      '#title' => $this->t('VIdeo ID'),
      '#description' => $this->t('Insert the video ID'),
      '#default_value' => $this->configuration['video']['src'] ?? '',
    ];
    $form['video_controls'] = [
      '#type' => 'checkbox',
      '#title' => $this
        ->t('Disable video controls'),
      '#default_value' => $this->configuration['video']['controls'] ?? '',
    ];

    $form['height'] = [
      '#type' => 'number',
      '#title' => $this
        ->t('Height'),
      '#default_value' => $this->configuration['video']['height'] ?? '',
    ];

    $form['width'] = [
      '#type' => 'number',
      '#title' => $this
        ->t('Width'),
      '#default_value' => $this->configuration['video']['width'] ?? '',
    ];

    $form['styles'] = [
      '#type' => 'textfield',
      '#title' => $this
        ->t('Class attributes'),
      '#default_value' => $this->configuration['video']['att'] ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    if ($values['width'] < 0) {
      $form_state->setErrorByName('width', $this->t('Invalid number'));
    }
    if ($values['height'] < 0) {
      $form_state->setErrorByName('height', $this->t('Invalid number'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->configuration['video']['src'] = $values['video_id'];
    $this->configuration['video']['controls'] = $values['video_controls'];
    $this->configuration['video']['height'] = $values['height'];
    $this->configuration['video']['width'] = $values['width'];
    $this->configuration['video']['att'] = $values['styles'];
  }

}

<?php

namespace Drupal\video_toolbox\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an Example entity.
 */
interface VideoStylesEntityInterface extends ConfigEntityInterface {

  /**
   * Get video width.
   */
  public function getWidth();

  /**
   * Set the video Width.
   */
  public function setWidth($width);

  /**
   * Get video height.
   */
  public function getHeight();

  /**
   * Set video height.
   */
  public function setHeight($height);

  /**
   * Get video attributes.
   */
  public function getVideoAtt();

  /**
   * Set video attributes.
   */
  public function setVideoAtt($attributes);

}

<?php

namespace Drupal\video_toolbox\Entity\ListBuilder;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Example.
 */
class VideoListStylesBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Video style');
    $header['id'] = $this->t('Machine name');
    $header['attributes'] = $this->t('Attributes');
    $header['width'] = $this->t('Width');
    $header['height'] = $this->t('Height');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\video_toolbox\Entity\VideoStylesEntityInterface $entity */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['attributes'] = $entity->getVideoAtt();
    $row['width'] = $entity->getWidth();
    $row['height'] = $entity->getHeight();

    // You probably want a few more properties here...
    return $row + parent::buildRow($entity);
  }

}

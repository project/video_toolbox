<?php

namespace Drupal\video_toolbox\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the VideoRatios entity.
 *
 * @ConfigEntityType(
 *   id = "video_toolbox_styles",
 *   label = @Translation("Video Ratios"),
 *   handlers = {
 *     "list_builder" = "Drupal\video_toolbox\Entity\ListBuilder\VideoListStylesBuilder",
 *     "form" = {
 *       "add" = "Drupal\video_toolbox\Form\VideoStyles",
 *       "edit" = "Drupal\video_toolbox\Form\VideoStyles",
 *       "delete" = "Drupal\video_toolbox\Form\VideoStylesDelete",
 *     }
 *   },
 *   config_prefix = "video_toolbox_styles",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "width" = "width",
 *     "height" = "height",
 *     "attributes" = "attributes",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "width",
 *     "height",
 *     "attributes",
 *   },
 *   links = {
 *     "add-form" = "/admin/config/media/video_toolbox_styles/add",
 *     "edit-form" = "/admin/config/media/video_toolbox_styles/{video_toolbox_styles}/edit",
 *     "delete-form" = "/admin/config/video_toolbox_styles/{video_toolbox_styles}/delete",
 *   }
 * )
 */
class VideoStylesEntity extends ConfigEntityBase implements VideoStylesEntityInterface {

  /**
   * The VideoRatios ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The VideoRatios label.
   *
   * @var string
   */
  protected $label;

  /**
   * The VideoRatios width.
   *
   * @var int
   */
  protected $width;

  /**
   * The VideoRatios height.
   *
   * @var int
   */
  protected $height;

  /**
   * The VideoRatios attributes.
   *
   * @var string
   */
  protected $attributes;

  /**
   * {@inheritDoc}
   */
  public function getWidth() {
    return $this->get('width');
  }

  /**
   * {@inheritDoc}
   */
  public function setWidth($width) {
    $this->set('width', $width);
    return $this;

  }

  /**
   * {@inheritDoc}
   */
  public function getHeight() {
    return $this->get('height');
  }

  /**
   * {@inheritDoc}
   */
  public function setHeight($height) {
    $this->set('height', $height);
    return $this;

  }

  /**
   * {@inheritDoc}
   */
  public function getVideoAtt() {
    return $this->get('attributes');
  }

  /**
   * {@inheritDoc}
   */
  public function setVideoAtt($attributes) {
    $this->set('attributes', $attributes);
    return $this;

  }

}

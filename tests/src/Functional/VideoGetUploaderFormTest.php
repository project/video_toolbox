<?php

namespace Drupal\Tests\video_toolbox\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests for the existence of the video upload form.
 *
 * @group video_toolbox
 */
class VideoGetUploaderFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['video_toolbox'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected $profile = 'standard';

  /**
   * A user with the 'Administer quick_node_clone' permission.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // Create admin user.
    $this->adminUser = $this->createUser([
      'access content',
    ], 'admin_test', TRUE);
  }

  /**
   * Test to see if the form is rendered properly.
   */
  public function testFormStructure() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('upload_video');
    $assert_session = $this->assertSession();
    $assert_session->fieldExists('video_desc');
    $assert_session->hiddenFieldExists('key');
    $assert_session->fieldExists('files[file]');
    $assert_session->hiddenFieldExists('uid');
    $assert_session->buttonExists('Submit');
  }

  /**
   * Test to see if the form is rendered properly.
   */
  public function testFormSubmit() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('upload_video');
    $assert_session = $this->assertSession();
    $assert_session->statusCodeEquals(200);

    $edit = [
      'video_desc' => 'ssds',
    ];

    $this->submitForm($edit, 'Submit');

    $assert_session->statusCodeEquals(200);

    $assert_session->pageTextContains('field is required.');
    $videoPath = \Drupal::service('extension.list.module')->getPath('video_toolbox') . '/tests/src/files/test.mp4';

    $edit = [
      'video_desc' => 'pog',
      'files[file]' => \Drupal::service('file_system')->realpath($videoPath),
    ];

    $this->submitForm($edit, 'Submit');

    $assert_session->statusCodeEquals(200);

    $assert_session->pageTextContains('Video successfully saved.');
  }

}

(function ($, Drupal, once) {
  "use strict";
  //Drupal behaviours
  Drupal.behaviors.video_toolbox = {
    attach: function (context, settings) {
      // Only execute code once. Without this wrapping function it would execute
      // as many times as called.
      once('video_toolbox', 'main', context).forEach(function (element) {
        // Configure element attributes as needed.
        $('.video-frame').each(function (i, obj) {
          let controls = $(this).siblings('.video-controls');
          if (controls.length) {
            $(this).removeAttr('controls');
            let volume = $(this).get(0).volume;
            let volumeBar = controls.find('.video-volume-bar');
            volumeBar.length ? volumeBar.get(0).value = (volume * 100) : 0
          }
        });
        $('video').attr('oncontextmenu', 'return false');

        // Give CSS properties.
        $('.fa-pause').css('display', 'none');
        $('.fa-volume-mute').css('display', 'none');

        // Add the configured color to the Video Player elements
        $('.video-controls').css('background', settings.videoToolBox.control_color).click(function (e) {
          // Stops Forms from being sent.
          e.preventDefault();
        });
        $('.video-bar').css('background', settings.videoToolBox.bar_color)
        $('.video-inner').css('background', settings.videoToolBox.progress_color)
        $('.video-timeline').css('border-color', settings.videoToolBox.progress_color)
        $('.video-player .video-controls button').css('color', settings.videoToolBox.button_color)

        // VIDEO CONTROL FUNCTIONS.
        // Update the progress bar.
        $('.video-frame').on('timeupdate', function () {
          let videoFrame = $(this).get(0);
          let videoControls = $(this).siblings('.video-controls');
          let curr = (videoFrame.currentTime / videoFrame.duration) * 100;
          if (videoFrame.ended) {
            videoControls.find(".fa-play").css('display', 'block');
            videoControls.find(".fa-pause").css('display', 'none');
          }
          videoControls.find('.video-inner').css('width', `${curr}%`);
        })

        // Play or Pause video (Pause button).
        $('.video-play-btn').click(function (e) {
          // Select the parent HTML5 video from the DOM.
          let video = $(this).parents('.video-controls').siblings('.video-frame').get(0);
          // Condition when to play a video.
          if (video.paused) {
            $(this).find('.fa-play').css('display', 'none');
            $(this).find('.fa-pause').css('display', 'block');
            video.play();
          }
          else {
            $(this).find('.fa-play').css('display', 'block');
            $(this).find('.fa-pause').css('display', 'none');
            video.pause();
          }
        });

        // Play or Pause video (Video Frame).
        $('.video-frame').click(function (e) {
          // Select the parent HTML5 video from the DOM.
          let video = $(this).get(0);
          // Get .video-controls sibling element.
          let controls = $(this).siblings('.video-controls');
          // Condition when to play a video.
          if (video.paused) {
            controls.find('.fa-play').css('display', 'none');
            controls.find('.fa-pause').css('display', 'block');
            video.play();
          }
          else {
            controls.find('.fa-play').css('display', 'block');
            controls.find('.fa-pause').css('display', 'none');
            video.pause();
          }
        });

        // Rewind the video.
        $('.video-backward-btn').click(function (e) {
          // Select the parent HTML5 video from the DOM.
          let video = $(this).parents('.video-controls').siblings('.video-frame').get(0);
          video.currentTime = video.currentTime - ((video.duration / 100) * 5);
        });

        // Fast forward video.
        $('.video-forward-btn').click(function (e) {
          // Select the parent HTML5 video from the DOM.
          let video = $(this).parents('.video-controls').siblings('.video-frame').get(0);
          video.currentTime = video.currentTime + ((video.duration / 100) * 5);
        });

        // Trigger Fullscreen.
        $('.video-expand-btn').click(function (e) {
          // Select the parent HTML5 video from the DOM.
          let videoJq = $(this).parents('.video-controls').siblings('.video-frame');
          let video = videoJq.get(0);
          let container = $(this).closest('.video-player').get(0);
          e.preventDefault();
          $(this).toggleClass('fullscreen-toggle');
          // Check compatibility with browsers.
          /* Chrome, Safari & Opera */
          if ($.isFunction(video.webkitEnterFullscreen)) {
            //videoJq.toggleClass('fullscreen-video');
            if ($(this).hasClass("fullscreen-toggle")) {
              container.webkitRequestFullScreen();
            } else {
              document.webkitCancelFullScreen();
            }
          }
          /* Firefox */
          else if ($.isFunction(video.mozRequestFullScreen)) {
            //videoJq.toggleClass('fullscreen-video');
            if ($(this).hasClass("fullscreen-toggle")) {
              container.mozRequestFullScreen();
            } else {
              document.mozCancelFullScreen();
            }
          }
          /* IE/Edge */
          else if ($.isFunction(video.msRequestFullscreen)) {
            //videoJq.toggleClass('fullscreen-video');
            if ($(this).hasClass("fullscreen-toggle")) {
              container.msRequestFullscreen();
            } else {
              document.msExitFullscreen();
            }
          }
          else {
            alert('Your browsers doesn\'t support fullscreen');
          }
        });

        $(document).on('mozfullscreenchange webkitfullscreenchange fullscreenchange', function () {
          let expandBtn = $(this).find('.fullscreen-toggle');
          let fullScreenVideo = $(this).find('.fullscreen-video');
          let videoJq = expandBtn.closest('.video-player').find('.video-frame');
          // Depending on the current state of the fullscreen, change it.
          // This takes into account leaving fullscreen by both the button
          // and keypress.
          if (expandBtn.length && fullScreenVideo.length) {
            fullScreenVideo.toggleClass('fullscreen-video');
            expandBtn.toggleClass('fullscreen-toggle');
          }
          else if (!expandBtn.length && fullScreenVideo.length) {
            fullScreenVideo.toggleClass('fullscreen-video');
          }
          else if (expandBtn.length) {
            videoJq.toggleClass('fullscreen-video');
          }
        });

        // Update the button icon on VOLUME change..
        $('.video-frame').on('volumechange', function () {
          let videoControls = $(this).siblings('.video-controls');
          if ($(this).get(0).volume == 0) {
            videoControls.find(".fa-volume-up").css('display', 'none');
            videoControls.find(".fa-volume-mute").css('display', 'block');
          } else {
            videoControls.find(".fa-volume-up").css('display', 'block');
            videoControls.find(".fa-volume-mute").css('display', 'none');
          }
        });

        function volumeBarDisplay(e, status) {
          let videoPlayer = e.closest('.video-player');
          let volumeBar = videoPlayer.find('.video-volume-bar');
          volumeBar.css('display', status);
        }

        // Show volume bar.
        $('.video-volume-btn').mouseenter(function () {
          volumeBarDisplay($(this), 'block');
        });
        // Stop displaying sound bar.
        $('.video-volume-bar').mouseleave(function () {
          volumeBarDisplay($(this), 'none');
        });
        $('.video-player').mouseleave(function () {
          volumeBarDisplay($(this), 'none');
        });
        $('.video-volume-btn').click(function () {
          let volumeBar = $(this).siblings('.video-volume-bar');
          if (volumeBar.css('display') === 'none') {
            volumeBarDisplay($(this), 'block');
          } else {
            volumeBarDisplay($(this), 'none');
          }
        })

        // Update video volume as specified.
        $('.video-volume-bar').click(function (e) {
          // Select the parent HTML5 video from the DOM.
          let videoFrame = $(this).parents('.video-controls').siblings('.video-frame').get(0);

          // Get the target (.video-bar).
          const target = e.currentTarget;

          // Get the bounding rectangle of target.
          const rect = target.getBoundingClientRect();

          // Mouse position relative to bar, and bar width.
          const x = e.clientX - rect.left;
          const rectWidth = rect.width;

          // Get the percentage of the selection to find the selected time.
          let selectVolume = (x / rectWidth).toFixed(2);
          let volumeVar = selectVolume * 100;
          videoFrame.volume = selectVolume;
          $(this).get(0).value = volumeVar;
          if ($(this).get(0).value > 94) {
            $(this).get(0).value = 100;
            videoFrame.volume = 1;
          } else if ($(this).get(0).value < 6) {
            $(this).get(0).value = 0;
            videoFrame.volume = 0;
          }
        });

        // Change video playbackrate.
        $('.video-playbackrate-btn').click(function () {
          let video = $(this).parent().siblings('.video-frame').get(0);
          let rateBtn = $(this);
          switch (video.playbackRate) {
            case 0.5:
              rateBtn.html('1X');
              video.playbackRate = 1;
              break;
            case 1:
              rateBtn.html('1.25X');
              video.playbackRate = 1.25;
              break;
            case 1.25:
              rateBtn.html('1.5X');
              video.playbackRate = 1.5;
              break;
            case 1.5:
              rateBtn.html('2X');
              video.playbackRate = 2;
              break;
            case 2:
              rateBtn.html('0.5X');
              video.playbackRate = 0.5;
              break;
            default:
              throw 'Invalid playbackrate';
          }
        });

        // Go to specified part of the video.
        $('.video-bar').click(function (e) {
          // Select the parent HTML5 video from the DOM.
          let videoFrame = $(this).parents('.video-controls').siblings('.video-frame').get(0);

          // Get the target (.video-bar).
          const target = e.currentTarget;

          // Get the bounding rectangle of target.
          const rect = target.getBoundingClientRect();

          // Mouse position relative to bar, and bar width.
          const x = e.clientX - rect.left;
          const rectWidth = rect.width;

          // Get the percentage of the selection to find the selected time.
          let currSelectPer = ((x / rectWidth) * 100).toFixed(2);
          let currSelect = (currSelectPer / 100) * (videoFrame.duration);
          videoFrame.currentTime = currSelect;
        });
      });
    }
  };
})(jQuery, Drupal, once);

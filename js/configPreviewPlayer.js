(function ($, Drupal, once) {
  "use strict";
  //Drupal behaviours
  Drupal.behaviors.video_toolbox = {
    attach: function (context, settings) {
      // Only execute code once. Without this wrapping function it would execute
      // as many times as called.
      once('video_toolbox', 'main', context).forEach(function (element) {

        const idCfgFormPrefix = 'edit-active-controls-';
        const buttonsInfo = {
          'play': ['btn-play', ['video-play-btn']],
          'fwrdback': ['btn-fwrdback', ['video-backward-btn', 'video-forward-btn']],
          'controlBar': ['control-bar', ['video-timeline']],
          'fullscreen': ['btn-fullscreen', ['video-expand-btn']],
          'speed': ['btn-speed', ['video-playbackrate-btn']],
          'sound': ['btn-sound', ['video-volume-btn', 'video-volume-bar']],
        };
        // Give CSS.
        $('.video-frame').css('background', '#262e33')
        $('.video-inner').css('width', '30%')
        $('.video-controls').click(function (e) {
          // Stops Forms from being sent.
          e.preventDefault();
        });
        $('.fa-pause').css('display', 'none');
        $('.fa-volume-mute').css('display', 'none');
        if ($('.video-volume-bar').length) {
          $('.video-volume-bar').css('display', 'block').get(0).value = 40;
        }

        // Change the preview styles when picking a color.
        $('#edit-default-control-color-0').on('change', function () {
            $('.video-controls').css('background', 'none');
        })
        $('#edit-default-control-color-1').on('change', function () {
          $('.video-controls').css('background', $('#edit-control-color').val());
        })
        $('#edit-control-color').on('change', function () {
          $('.video-controls').css('background', $(this).val());
        })
        $('#edit-bar-color').on('change', function () {
          $('.video-bar').css('background', $(this).val());
        })
        $('#edit-progress-color').on('change', function () {
          $('.video-inner').css('background', $(this).val());
          $('.video-timeline').css('border-color', $(this).val());
        })
        $('#edit-button-color').on('change', function () {
          $('.video-player .video-controls button').css('color', $(this).val());
        })

        // BUTTON VISIBILITY.
        // Change button visibility
        function changeVisibility(target) {
          target.forEach(targetClass => {
            let targetElement = $(`.${targetClass}`);
            if (targetElement.css('display') == 'none') {
              targetElement.css('display', 'block');
              if (targetClass) {
                targetElement.css('display', 'flex')
              }
            } else {
              targetElement.css('display', 'none');
            }
          });
        }

        //
        $('#edit-active-controls').change(function (e) {
          switch (e.target.id) {
            case idCfgFormPrefix + buttonsInfo.play[0]:
              changeVisibility(buttonsInfo.play[1]);
              break;
            case idCfgFormPrefix + buttonsInfo.fwrdback[0]:
              changeVisibility(buttonsInfo.fwrdback[1]);
              break;
            case idCfgFormPrefix + buttonsInfo.controlBar[0]:
              changeVisibility(buttonsInfo.controlBar[1]);
              break;
            case idCfgFormPrefix + buttonsInfo.fullscreen[0]:
              changeVisibility(buttonsInfo.fullscreen[1]);
              break;
            case idCfgFormPrefix + buttonsInfo.speed[0]:
              changeVisibility(buttonsInfo.speed[1]);
              break;
            case idCfgFormPrefix + buttonsInfo.sound[0]:
              changeVisibility(buttonsInfo.sound[1]);
              break;

            default:
              break;
          }
        });

        // SET INITIAL PREVIEW STYLES.
        // Add the configured color to the Video Player elements.
        $('.video-controls').css('background', settings.videoToolBox.control_color);
        $('.video-bar').css('background', settings.videoToolBox.bar_color);
        $('.video-inner').css('background', settings.videoToolBox.progress_color);
        $('.video-timeline').css('border-color', settings.videoToolBox.progress_color);
        $('.video-player .video-controls button').css('color', settings.videoToolBox.button_color);

        // Display the controls depending on if they are activated or not.
        const settingActiveControls = settings.videoToolBox.active_controls;
        let activeControls = [];
        for (const key in settingActiveControls) {
          if (settingActiveControls[key] === true) {
            activeControls[key] = 'block';
            if (key === 'control_bar') {
              activeControls[key] = 'flex';
            }
          } else {
            activeControls[key] = 'none';
          }
        }

        // Show / Hide each button.
        $('.video-play-btn').css('display', activeControls['btn_play']);
        $('.video-backward-btn').css('display', activeControls['btn_fwrdback']);
        $('.video-forward-btn').css('display', activeControls['btn_fwrdback']);
        $('.video-timeline').css('display', activeControls['control_bar']);
        $('.video-expand-btn').css('display', activeControls['btn_fullscreen']);
        $('.video-playbackrate-btn').css('display', activeControls['btn_speed']);
        $('.video-volume-btn').css('display', activeControls['btn_sound']);
        $('.video-volume-bar').css('display', activeControls['btn_sound']);
      });
    }
  };
})(jQuery, Drupal, once);

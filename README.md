# Table of Contents

 - [Introduction](#introduction)
 - [Features](#features)
 - [Requirements](#requirements)
 - [Installation](#installation)
 - [Usage/Configuration](#usageconfiguration)
 - [Supporting organizations](#supporting-organizations)
 - [Maintainers](#maintainers)

## Introduction

Video toolbox is designed to be module with a variety of tools
that will help you to organize, share, decorate,
moderate and manage your videos in an easy way.

Also we aim to have full support in video conversion and styling.
In the next part I will know all the functionalities that this
modules offers at the moment.

## Features
- Upload videos and watch videos.
- Easily see all uploaded videos in a report and search them by user.
- Video Field Type.
- Custom video player
- Edit the properties of uploaded videos.
- Download uploaded videos.
- Useful video blocks
- Import past videos to the module
- Private videos
- Basic styles (only works with classes for now)

## Requirements
No special requirements.

## Installation
Normal module installation through Composer / Version History.
Once the module is in its proper modules directory, finish installing it by
going to the "Extend" tab in the admin pages, search for "Video ToolBox"
under the "Tools" category.

[More information](https://www.drupal.org/docs/extending-drupal/installing-modules)

## Usage/Configuration
If you wanna to import all you videos in files and change
the extensions allowed, please follow the next steps:

1. Go to Configuration > Media > Video ToolBox Settings > Import all videos.
2. Check for allowed extensions that you would like to add.

#### Supporting organizations
- [CI&T](https://www.drupal.org/cit)

#### Maintainers
 - [LeoAlcci](https://www.drupal.org/u/leoalcci)
 - [Jacoboa](https://www.drupal.org/u/jacoboa)
 - [Arturo1007](https://www.drupal.org/u/arturo1007)

This module is under active development.
Created on **21 October 2022**.
